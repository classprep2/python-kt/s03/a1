# [Section] Classes
class Camper():
	# set constructor
	def __init__(self, name, batch, course_type):
		self.name = name
		self.batch = batch
		self.course_type = course_type

	# returns the object representation in string format
	# def __repr__(self):
	# 	# return f"My car is {self.brand} {self.model}"

	# methods
	def career_track(self):
		print(f"Currently enrolled in the {self.course_type} program.")

	def info(self):
		print(f"My name is {self.name} of batch {self.batch}.")

	def zuitt_camper(self):
		print(f"Camper Name: {self.name}")
		print(f"Camper Batch: {self.batch}")
		print(f"Camper Course: {self.course_type}")

new_camper = Camper("Alan", 100, "Pyhton Short Course")
new_camper.zuitt_camper()
new_camper.info()
new_camper.career_track()